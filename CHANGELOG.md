# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## v0.1.0 - 2019-03-09

### Added

- This project.
- [EditorConfig](https://editorconfig.org/) integration.
- [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/) integration.
- [Storybook](https://storybook.js.org) integration.
- [Gitlab CI](https://about.gitlab.com/product/continuous-integration/) integration.

